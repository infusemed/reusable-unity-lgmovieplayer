#pragma strict

var guiDepth :int = 0;
var showPlaybackIndicator :boolean = true;
var currentTime :float = 0;

private var movieRect :Rect;
private var moviePlay :boolean;
private var audioSource :AudioSource;
private var pixelBlackTexture :Texture2D;
private var pixelGrayTexture :Texture2D;

function Start () {
	pixelBlackTexture = Resources.Load("UI/pixelColor/pixelBlack");
	pixelGrayTexture = Resources.Load("UI/pixelColor/pixelGray");
}

function Update () 
{

}

function OnGUI () 
{
	GUI.depth = guiDepth;
	OnGUIFunction();
}

#if UNITY_IPHONE
//iOS functions
function PlayMovie (movie)
{
	var moviePath = movie.ToString() +".mp4";
}

function OnGUIFunction()
{

}



#else
//StandAlone functions
private var movieTexture :MovieTexture;
function PlayMovie (movie :MovieTexture, rect :Rect) 
{
	movieRect = rect;
	if (movie) {
		movieTexture = movie;
		movieTexture.Play();
		if (movie.audioClip) {
			audioSource = gameObject.AddComponent(AudioSource);
			audioSource.rolloffMode = AudioRolloffMode.Linear;
			audioSource.audio.clip = movie.audioClip;
			audioSource.Play();
		}
		currentTime = 0;
		moviePlay = true;
	}
}

function OnGUIFunction() 
{
    if (moviePlay) {
    	currentTime += Time.deltaTime*0.5; //deltatime seems a little fast
    
    	GUI.DrawTexture(movieRect, pixelBlackTexture, ScaleMode.StretchToFill, false, 0);
	    GUI.DrawTexture(movieRect, movieTexture, ScaleMode.ScaleToFit, false, 0); 
	    
	    var playbackCurrentWidth = currentTime/movieTexture.duration * movieRect.width;
	    var playbackRect = Rect(movieRect.x, movieRect.y+movieRect.height-10, playbackCurrentWidth, 10);
	    GUI.DrawTexture(Rect(movieRect.x, movieRect.y+movieRect.height-12, movieRect.width, 12), pixelBlackTexture, ScaleMode.StretchToFill, false, 0);
	    GUI.DrawTexture(playbackRect, pixelGrayTexture, ScaleMode.StretchToFill, false, 0);
	    
	    if (GUI.Button(movieRect, "", GUIStyle.none) || !movieTexture.isPlaying) {
	    	moviePlay = false;
			movieTexture.Stop();
			if (audioSource) {
				audioSource.audio.Stop();
				Destroy(audioSource);
			}
			Destroy(this);
	    }  	
    }
}
#endif